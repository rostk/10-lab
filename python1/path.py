class Coffee_van :
    weight = 0

    def __init__(self,brand_name="default",price=0,new_weight = 0, quality="default"):
        self.brand_name=brand_name
        self.price=price
        self.new_weight=new_weight
        self.quality=quality
        Coffee_van.weight +=new_weight

    def to_string(self):
        print("Name of brand: " + self.brand_name + ", Price:", self.price,
              ", Weight:", self.new_weight, ", Quality:" + self.quality + ";")

    def print_sum(self):
        print("Вага : " + self.brand_name + ": ", self.new_weight)

    @staticmethod
    def print_static_sum():
        print("Загальна вага : ", Coffee_van.weight)

if __name__ == '__main__':
    GroundCoffee = Coffee_van()
    BlackCoffee = Coffee_van("Eat-Injetto", 12.50, 200, "nice")
    Latte = Coffee_van("Malongo", 20.56, 228, "perfect")

    print("\n")
GroundCoffee.to_string()
BlackCoffee.to_string()
Latte.to_string()

print("\n")
GroundCoffee.print_sum()
BlackCoffee.print_sum()
Latte.print_sum()

print("\n")
Coffee_van.print_static_sum()
